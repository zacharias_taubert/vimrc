echo 'set runtimepath+=~/.vim_runtime
execute pathogen#infect()

source ~/.vim_runtime/vimrcs/basic.vim
source ~/.vim_runtime/vimrcs/extended.vim
source ~/.vim_runtime/vimrcs/plugins_config.vim

source ~/.vim_runtime/user.vim' > ~/.vimrc

echo 'Done!'
