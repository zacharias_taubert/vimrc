# vimrc

## Creds
Most of this vimrc has been copied from [amix vimrc](https://github.com/amix/vimrc).

## Installation
###SSH
```
git clone git@bitbucket.org:zacharias_taubert/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install.sh
```
