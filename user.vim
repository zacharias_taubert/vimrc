set shiftwidth=4 tabstop=4 softtabstop=4 smartindent autoindent expandtab smarttab
let NERDTreeQuitOnOpen=1

au BufNewfile,BufRead *.yml
    \ set tabstop=2 softtabstop=2 shiftwidth=2

au BufNewFile,BufRead *.py
    \ set tabstop=4 softtabstop=4 shiftwidth=4 nosmartindent
